package silverblaze

import (
	"bytes"
	"encoding/base64"
	"encoding/xml"
	"github.com/joho/godotenv"
	"io/ioutil"
	"log"
	"net/http"
	"os"
)

type SoapEnvelope struct {
	Body body `xml:"Body"`
}

type body struct {
	Response commandResponse `xml:"executeCommandResponse"`
}

type commandResponse struct {
	Result string `xml:"result"`
}

var SoapUser string;
var SoapPassword string;

func getCommandBody(command string) (string) {
	command_template := "<SOAP-ENV:Envelope xmlns:SOAP-ENV='http://schemas.xmlsoap.org/soap/envelope/' xmlns:ns1='urn:TC' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:SOAP-ENC='http://schemas.xmlsoap.org/soap/encoding/' SOAP-ENV:encodingStyle='http://schemas.xmlsoap.org/soap/encoding/'>" +
		"<SOAP-ENV:Body><ns1:executeCommand>" +
		"<command xsi:type='xsd:string'>" +
		command +
		"</command>" +
		"</ns1:executeCommand>" +
		"</SOAP-ENV:Body>" +
		"</SOAP-ENV:Envelope>"

	return command_template
}

type CommandResponse struct {
	Body string
	Status int
}

func DoCommand(command string) (CommandResponse) {

	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}
	username := SoapUser
	password := SoapPassword

	authorization := base64.StdEncoding.EncodeToString([]byte(username + ":" + password))

	SoapUrl := os.Getenv("soap_url")

	client := &http.Client{}
	req, err := http.NewRequest("POST", SoapUrl, bytes.NewBufferString(getCommandBody(command)))

	if err != nil {
		log.Fatalln(err)
	}

	req.Header.Set("Authorization", "Basic " + authorization)
	req.Header.Set("SOAPAction", "urn:TC#executeCommand")
	req.Header.Set("Content-Type", "application/xml")

	resp, err := client.Do(req)

	if(err != nil) {
		log.Fatalln(err)
	}

	//log.Println(resp.StatusCode)
	bodyBytes, _ := ioutil.ReadAll(resp.Body)

	var envelope SoapEnvelope;
	err2 := xml.Unmarshal([]byte(bodyBytes), &envelope)

	if err2 != nil {
		log.Fatalln(err)
	}

	return CommandResponse{Body : envelope.Body.Response.Result, Status : resp.StatusCode}
}
