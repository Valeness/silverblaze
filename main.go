package main

import (
	"html/template"
	"io"
	"log"
	"net/http"
	"silverblaze/silverblaze"
)

type Page struct {
	Title string
}

func indexHandler(w http.ResponseWriter, r *http.Request) {
	p := Page{Title : "Silverblaze"};
	renderTemplate(w, "index", &p)
}

func dashHandler(w http.ResponseWriter, r *http.Request) {
	p := Page{Title : "Silverblaze - Dashboard"};
	renderTemplate(w, "dash", &p)
}

func commandHandler(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	command := r.PostForm.Get("command")

	user := r.PostForm.Get("username")
	password := r.PostForm.Get("password")

	//log.Println(user, password)

	silverblaze.SoapUser = user
	silverblaze.SoapPassword = password
	resp := silverblaze.DoCommand(command)
	w.WriteHeader(resp.Status)
	io.WriteString(w, resp.Body)
}

func renderTemplate(w http.ResponseWriter, tmpl string, p *Page) {
	t, _ := template.ParseFiles("templates/master.html", "templates/" + tmpl + ".html")
	t.ExecuteTemplate(w, "master", p)
}

func main() {
	http.HandleFunc("/", indexHandler)
	http.HandleFunc("/command", commandHandler)
	http.HandleFunc("/dash", dashHandler)
	http.Handle("/assets/", http.StripPrefix("/assets/", http.FileServer(http.Dir("assets"))))
	log.Fatal(http.ListenAndServe(":8080", nil))
}